import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'cxcsfdsfsdasdas sadasd'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
                              'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DEBUG = True
    MAIL_SERVER = os.environ.get('MAIL_SERVER', 'smtp.gmail.com')
    MAIL_PORT = int(os.environ.get('MAIL_PORT', '587'))
    MAIL_USE_TLS = os.environ.get('MAIL_USE_TLS', 'true').lower() in \
                   ['true', 'on', '1']
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME', 'Trucking')
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD', 'G1wDRx21')
    MAIL_SUBJECT_PREFIX = '[Trucking]'
    MAIL_SENDER = 'Trucking Admin <davrafil@gmail.com>'
    TRUCKING_ADMIN = os.environ.get('TRUCKING_ADMIN', 'dav-rafil@yandex.ru')
    UPLOADED_PHOTOS_DEST = os.environ.get('UPLOADED_PHOTOS_DEST', 'app/static/upload')
    TRUCKING_POSTS_PER_PAGE = os.environ.get('TRUCKING_POSTS_PER_PAGE', 10)
    TRUCKING_OFFERS_PER_PAGE = os.environ.get('TRUCKING_OFFERS_PER_PAGE', 10)
    TRUCKING_PROCENT = os.environ.get('TRUCKING_PROCENT', 2)

    @staticmethod
    def init_app(app):
        pass
