from random import randint
from sqlalchemy.exc import IntegrityError
from faker import Faker
from . import db
from .models import User, Ad


def users(count=10):
    fake = Faker()
    i = 0
    while i < count:
        u = User(email=fake.email(),
                 username=fake.user_name(),
                 password='password',
                 confirmed=True,
                 usertype_id= 1 if i%2==0 else 2,
                 name=fake.name(),
                 location=fake.city(),
                 about_me=fake.text())
        db.session.add(u)
        try:
            db.session.commit()
            i += 1
        except IntegrityError:
            db.session.rollback()


# def posts(count=100):
#     fake = Faker()
#     user_count = User.query.count()
#     for i in range(count):
#         u = User.query.offset(randint(0, user_count - 1)).first()
#         p = Ad(body=fake.text(),
#                  timestamp=fake.past_date(),
#                  author=u)
#         db.session.add(p)
#     db.session.commit()
