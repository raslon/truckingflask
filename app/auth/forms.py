from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, RadioField
from wtforms.validators import DataRequired, Length, Email, Regexp, EqualTo, ValidationError

from app.models import User


class LoginForm(FlaskForm):
    email = StringField('E-mail', validators=[DataRequired(), Length(1, 64), Email()])
    password = PasswordField('Пароль', validators=[DataRequired()])
    remember_me = BooleanField('Запомнить')
    submit = SubmitField('Войти')


class RegistrationForm(FlaskForm):

    email = StringField('Email', validators=[DataRequired(), Length(1, 64), Email()])
    username = StringField('Имя', validators=[DataRequired(), Length(1, 64),
                                              Regexp('^[A-Za-zА-Яа-я][A-Za-zА-Яа-я0-9]*$', 0,
                                                     'Имя пользователя должн состоять только из букв и цифр')])
    usertype = RadioField('Label', choices=[], default='Грузоотправитель')
    password = PasswordField('Пароль',
                             validators=[DataRequired(), EqualTo('password2', message='Пароли должны совпадать')])
    password2 = PasswordField('Повторите пароль', validators=[DataRequired()])
    submit = SubmitField('Зарегистрироваться')

    def validate_email(self, field):
        if User.query.filter_by(email=field.data.lower()).first():
            raise ValidationError('Этот Email уже используется')

    def validate_username(self, field):
        if User.query.filter_by(username=field.data).first():
            raise ValidationError('Имя пользователя уже используется')


class ChangePasswordForm(FlaskForm):
    old_password = PasswordField('Старый пароль', validators=[DataRequired()])
    password = PasswordField('Новый пароль', validators=[
        DataRequired(), EqualTo('password2', message='Пароли должны совпадать')])
    password2 = PasswordField('Повторите пароль',
                              validators=[DataRequired()])
    submit = SubmitField('Сохранить новый пароль')


class PasswordResetRequestForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Length(1, 64),
                                             Email()])
    submit = SubmitField('Сбосить пароль')


class PasswordResetForm(FlaskForm):
    password = PasswordField('Новый пароль', validators=[
        DataRequired(), EqualTo('password2', message='Пароли должны совпадать')])
    password2 = PasswordField('Повторите пароль', validators=[DataRequired()])
    submit = SubmitField('Сбросить пароль')


class ChangeEmailForm(FlaskForm):
    email = StringField('Новый Email', validators=[DataRequired(), Length(1, 64),
                                                 Email()])
    password = PasswordField('Пароль', validators=[DataRequired()])
    submit = SubmitField('Сохранить новый E-mail')

    def validate_email(self, field):
        if User.query.filter_by(email=field.data.lower()).first():
            raise ValidationError('Электронная почта уже зарегистрирована')