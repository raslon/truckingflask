import os
from datetime import datetime

from flask import render_template, redirect, url_for, abort, flash, current_app, request
from flask_login import login_required, current_user
from werkzeug.utils import secure_filename

from . import main
from .forms import EditProfileForm, EditProfileAdminForm, AdForm, OfferForm
from .. import db
from ..models import Role, User, UserType, Permission, Ad, Offer, Execute
from ..decorators import admin_required


@main.route('/', methods=['GET', 'POST'])
def index():
    page = request.args.get('page', 1, type=int)
    exectude = Execute.query.order_by().all()
    list_exectude = tuple([x.ad_id for x in exectude])
    pagination = Ad.query.order_by(Ad.timestamp.desc()).filter( ~Ad.id.in_(list_exectude)).paginate(
        page, per_page=current_app.config['TRUCKING_POSTS_PER_PAGE'],
        error_out=False)
    ads = pagination.items
    return render_template('index.html', ads=ads, pagination=pagination)


@main.route('/create_task', methods=['GET', 'POST'])
@login_required
def create_task():
    form = AdForm()
    if current_user.can(Permission.WRITE) and form.validate_on_submit():

        ad = Ad(from_address=form.from_.data, to_address=form.to_.data, description=form.description.data, price=form.price.data, time_departure=form.time_departure.data, delivery_time=form.delivery_time.data,
                author=current_user._get_current_object())
        db.session.add(ad)
        db.session.commit()
        return redirect(url_for('.index'))
    ads = Ad.query.order_by(Ad.timestamp.desc()).all()
    return render_template('create_task.html', form=form, ads=ads)



@main.route('/user/<username>')
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    return render_template('user.html', user=user)


@main.route('/edit-profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    choies_ =[x.name for x in UserType.query.filter_by().all()]
    form = EditProfileForm()

    form.usertype.choices = choies_
    if form.validate_on_submit():
        usertype = UserType.query.filter_by(name=form.usertype.data).first()
        if usertype is None:
            flash('Ошибка при обновление данных')
            return render_template('edit_profile.html', form=form)
        current_user.name = form.name.data
        current_user.usertype_id=usertype.id
        current_user.location = form.location.data
        current_user.about_me = form.about_me.data

        ava =  form.avatar.data
        if ava:
            otherfile = secure_filename(ava.filename)
            date = "{:%I%M%S%f%d%m%Y}".format(datetime.now())
            otherfile = date + otherfile
            form.avatar.data.save(os.path.join(current_app.config['UPLOADED_PHOTOS_DEST'], otherfile))
            current_user.avatar=otherfile
        db.session.add(current_user._get_current_object())
        db.session.commit()
        flash('Ваш профиль обновлен.')
        return redirect(url_for('.user', username=current_user.username))
    form.name.data = current_user.name
    form.usertype.data = current_user.usertype.name
    form.location.data = current_user.location
    form.about_me.data = current_user.about_me

    ava = form.avatar.data
    if ava:
        otherfile = secure_filename(ava.filename)
        date = "{:%I%M%S%f%d%m%Y}".format(datetime.now())
        otherfile = date + otherfile
        form.avatar.data.save(os.path.join(current_app.config['UPLOADED_PHOTOS_DEST'], otherfile))
        current_user.avatar=otherfile
    return render_template('edit_profile.html', form=form)


@main.route('/edit-profile/<int:id>', methods=['GET', 'POST'])
@login_required
@admin_required
def edit_profile_admin(id):
    user = User.query.get_or_404(id)
    form = EditProfileAdminForm(user=user)
    if form.validate_on_submit():
        user.email = form.email.data
        user.username = form.username.data
        user.confirmed = form.confirmed.data
        user.role = Role.query.get(form.role.data)
        user.name = form.name.data
        user.location = form.location.data
        user.about_me = form.about_me.data
        ava = form.avatar.data
        if ava:
            otherfile = secure_filename(ava.filename)
            date = "{:%I%M%S%f%d%m%Y}".format(datetime.now())
            otherfile = date + otherfile
            form.avatar.data.save(os.path.join(current_app.config['UPLOADED_PHOTOS_DEST'], otherfile))
            user.avatar=otherfile
        db.session.add(user)
        db.session.commit()
        flash('Ваш профиль обновлен.')
        return redirect(url_for('.user', username=user.username))
    form.email.data = user.email
    form.username.data = user.username
    form.confirmed.data = user.confirmed
    form.role.data = user.role_id
    form.name.data = user.name
    form.location.data = user.location
    form.about_me.data = user.about_me

    ava = form.avatar.data
    if ava:
        otherfile = secure_filename(ava.filename)
        date = "{:%I%M%S%f%d%m%Y}".format(datetime.now())
        otherfile = date + otherfile
        form.avatar.data.save(os.path.join(current_app.config['UPLOADED_PHOTOS_DEST'], otherfile))
        user.avatar=otherfile
    return render_template('edit_profile.html', form=form, user=user)


@main.route('/task/<int:id>', methods=['GET', 'POST'])
def task(id):
    ad = Ad.query.get_or_404(id)

    if Execute.query.filter_by(ad_id=id).first():
        abort(404)
    form = OfferForm()
    offer = Offer()
    if form.validate_on_submit():
        exectude = Execute.query.filter_by(ad_id=id).first()
        if exectude:
            flash('Исполнитель уже выбран')
            return redirect(url_for('main.index'))

        if Offer.query.filter_by(author_id=current_user.id, ad_id=id).first():
            flash('Вы уже отправили свое предложение')
            return redirect(url_for('.task', id=ad.id))
        offer.body=form.body.data
        offer.ad_id=ad.id
        offer.author_id=current_user.id
        db.session.add(offer)
        db.session.commit()
        flash('Ваша предложение отправлено')
        return redirect(url_for('.task', id=ad.id, page=-1))
    page = request.args.get('page', 1, type=int)
    if page == -1:
        page = (ad.offers.count() - 1) // \
           current_app.config['TRUCKING_OFFERS_PER_PAGE'] + 1
    pagination = ad.offers.order_by(Offer.timestamp.asc()).paginate(page, per_page=current_app.config['TRUCKING_OFFERS_PER_PAGE'], error_out=False)
    offers = pagination.items
    return render_template('task.html', ad=ad, form=form, offers=offers, pagination=pagination)


@main.route('/edit/<int:id>', methods=['GET', 'POST'])
@login_required
def edit(id):
    ad = Ad.query.get_or_404(id)
    if current_user != ad.author and \
            not current_user.can(Permission.ADMIN):
        abort(403)
    form = AdForm()
    if form.validate_on_submit():
        ad.time_departure=form.time_departure.data
        ad.delivery_time=form.delivery_time.data
        ad.price=form.price.data
        ad.from_address=form.from_.data
        ad.to_address=form.to_.data
        ad.description = form.description.data
        db.session.add(ad)
        db.session.commit()
        flash('Задание обновлено')
        return redirect(url_for('.task', id=ad.id))
    form.to_.data=ad.to_address
    form.from_.data=ad.from_address
    form.price.data=ad.price
    form.delivery_time.data = ad.delivery_time
    form.time_departure.data=ad.time_departure
    form.description.data = ad.description
    return render_template('edit_task.html', form=form)

@main.route('/exectude/',  methods=['GET', 'POST'])
@login_required
def exectude():
    user_id = request.args.get('user_id')
    ad_id = request.args.get('ad_id')
    ad = Ad.query.get_or_404(ad_id)
    exec = Execute()
    if current_user != ad.author:
        abort(403)
    if exec.query.filter_by(ad_id=ad_id).first():
        user = User.query.filter_by(id=user_id).first()
        flash('Вы уже выбрали {}'.format(user.username))
        return redirect(url_for('.task', id=ad.id))

    exec.user_id = user_id
    exec.ad_id = ad_id
    price=ad.price

    user = User.query.filter_by(id=user_id).first()

    user.price = float(user.price) + (price-((price*current_app.config['TRUCKING_PROCENT'])/100))
    db.session.add(user)
    db.session.add(exec)
    db.session.commit()
    db.session.commit()
    flash('Перевозчик выбран')
    return redirect(url_for('main.index', id=ad.id))

@main.route('/mytasks/')
@login_required
def mytasks():
    exectude = Execute.query.filter_by(user_id=current_user.id).all()
    if exectude:
        page = request.args.get('page', 1, type=int)
        pagination = Ad.query.filter_by(id=Ad.id.in_(tuple([x.ad_id for x in exectude]))).paginate(
            page, per_page=current_app.config['TRUCKING_POSTS_PER_PAGE'],
            error_out=False)
        ads = pagination.items
    else:
        page = request.args.get('page', 1, type=int)
        pagination = Ad.query.filter_by(author_id=current_user.id).paginate(
            page, per_page=current_app.config['TRUCKING_POSTS_PER_PAGE'],
            error_out=False)
        ads = pagination.items
    return render_template('mytasks.html', ads=ads, pagination=pagination)