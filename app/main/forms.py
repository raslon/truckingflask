from flask_login import current_user
from flask_wtf import FlaskForm, validators
from flask_wtf.file import FileAllowed, FileRequired
from wtforms import StringField, TextAreaField, BooleanField, SelectField, \
    SubmitField, RadioField, FileField, FloatField
from wtforms.fields.html5 import IntegerField, DateTimeField, DateTimeLocalField
from wtforms.validators import DataRequired, Length, Email, Regexp, Required
from wtforms import ValidationError
from wtforms.widgets import html5

from ..models import Role, User


class AdForm(FlaskForm):
    from_ = StringField('Откуда', validators=[Length(0, 64)])
    to_ = StringField('Куда', validators=[Length(0, 64)])
    description = TextAreaField('Описание')
    price = FloatField('Цена', widget=html5.NumberInput(min=0, max=9999999))
    time_departure = DateTimeLocalField('Дата и время приема груза', format='%Y-%m-%dT%H:%M')
    delivery_time = DateTimeLocalField('Доставить до', format='%Y-%m-%dT%H:%M')
    submit = SubmitField('Создать')


class EditProfileForm(FlaskForm):
    name = StringField('Настоящее имя', validators=[Length(0, 64)])
    usertype = RadioField('Label', choices=[])
    location = StringField('Город', validators=[Length(0, 64)])
    about_me = TextAreaField('О себе')
    avatar = FileField('Аватар', validators=[FileAllowed(['jpg', 'png'], 'Images only!')])
    submit = SubmitField('Сохранить')


class EditProfileAdminForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Length(1, 64),
                                             Email()])
    username = StringField('Username', validators=[
        DataRequired(), Length(1, 64),
        Regexp('^[A-Za-z][A-Za-z0-9_.]*$', 0,
               'Usernames must have only letters, numbers, dots or '
               'underscores')])
    confirmed = BooleanField('Confirmed')
    role = SelectField('Role', coerce=int)
    name = StringField('Real name', validators=[Length(0, 64)])
    location = StringField('Location', validators=[Length(0, 64)])
    about_me = TextAreaField('About me')
    avatar = FileField(validators=[FileRequired(), FileAllowed(['jpg', 'png'], 'Аватар')])

    def __init__(self, user, *args, **kwargs):
        super(EditProfileAdminForm, self).__init__(*args, **kwargs)
        self.role.choices = [(role.id, role.name)
                             for role in Role.query.order_by(Role.name).all()]
        self.user = user

    def validate_email(self, field):
        if field.data != self.user.email and \
                User.query.filter_by(email=field.data).first():
            raise ValidationError('Email already registered.')

    def validate_username(self, field):
        if field.data != self.user.username and \
                User.query.filter_by(username=field.data).first():
            raise ValidationError('Username already in use.')


class OfferForm(FlaskForm):
    body = StringField('Сопроводительное письмо', validators=[Required()])
    submit = SubmitField('Пердложить')

class Execute(FlaskForm):
    submit = SubmitField('Выбрать')