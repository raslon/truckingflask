**Создание виртуального окружения**
---
```virtualenv venv```

***Активация виртуального окружения***

Для активации виртуального окружения воспользуйтесь командой (для Linux):

```source venv/bin/activate```

Для Windows команда будет выглядеть так:

```venv\Scripts\activate```

___

**Установка зависимостей**
---
```pip install -r requirements.txt```
___

**Миграция** ***(при необходимости)***
---
**Для Linux**

```export FLASK_APP=trucking.py```

```export FLASK_DEBUG=1```

**Для Microsoft Windows:**

```set FLASK_APP=trucking.py```

```set FLASK_DEBUG=1```

```flask db migrate -m "initial migration"```

```flask db upgrade```
___
**Confi.py**
---

```SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or 'sqlite:///' + os.path.join(basedir, 'app.db')``` Путь/URI базы данных, который будет использоваться для подключения

```DEBUG = True/False``` Включить/выключить режим отладки.

```SECRET_KEY = os.environ.get('SECRET_KEY') or 'cxcsfdsfsdasdas sadasd'``` Секретный ключ

Электронная почта для отправки подтверждений

```MAIL_SERVER = os.environ.get('MAIL_SERVER', 'smtp.gmail.com')``` Email сервер _(пример: smtp.gmail.com')_

```MAIL_PORT = int(os.environ.get('MAIL_PORT', '587'))``` порт Email сервера _(пример: 587)_

```MAIL_USE_TLS = os.environ.get('MAIL_USE_TLS', 'true').lower() in ['true', 'on', '1']``` Mail TLS

```MAIL_USERNAME = os.environ.get('MAIL_USERNAME', 'Trucking')```

```MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD', 'Email password')```

```MAIL_SUBJECT_PREFIX = '[Trucking]'```

```MAIL_SENDER = 'Trucking Admin <address@gmail.com>'``` 

```TRUCKING_ADMIN = os.environ.get('TRUCKING_ADMIN', 'address@mail.com')```  - Email администратора

```UPLOADED_PHOTOS_DEST = os.environ.get('UPLOADED_PHOTOS_DEST', 'app/static/upload')``` - путь к аватарам

```TRUCKING_POSTS_PER_PAGE = os.environ.get('TRUCKING_POSTS_PER_PAGE', 10)``` - количество объявлений

```TRUCKING_OFFERS_PER_PAGE = os.environ.get('TRUCKING_OFFERS_PER_PAGE', 10)``` - количество предложений

```TRUCKING_PROCENT = os.environ.get('TRUCKING_PROCENT', 2)``` - комиссия _(пример: 2%)_

___
**Запуск**
---
**Для Linux**

```export FLASK_APP=trucking.py```

```export FLASK_DEBUG=1```

**Для Microsoft Windows:**

```set FLASK_APP=trucking.py```

```set FLASK_DEBUG=1```

**Запуск**

```flask run```


**Пользователи для примера**
___

* nataliequinn@yahoo.com
* leealexa@yahoo.com перевозчик
* hodgeblake@gmail.com
* erichardy@hotmail.com перевозчик
* duanereid@wang.org
* kerriwilliams@hester.com перевозчик
* hwilliams@roth.biz
* davidbranch@gmail.com перевозчик
* kathleen19@hotmail.com
* smithbrandon@gmail.com перевозчик
____